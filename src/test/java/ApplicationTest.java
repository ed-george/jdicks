import uk.co.edgeorgedev.jdicks.Dicks;
import uk.co.edgeorgedev.jdicks.JDicks;
import uk.co.edgeorgedev.jdicks.exception.DontBeADickException;

/**
 * ApplicationTest.java
 * JDicks
 *
 * Created by Ed George on 7 Jan 2015
 *
 */

/**
 * @author edgeorge
 *
 */
public class ApplicationTest {

	public static void main(String[] args) {
		JDicks jdicks = new JDicks();
		
		try {
			System.out.println(jdicks.getDicks(2).toJSON());
			Dicks dicks = jdicks.getDicks(10);
			System.out.println(dicks);
			System.out.println(dicks.sort());
		} catch (DontBeADickException e) {
			e.printStackTrace();
		}
		
	}
	
}
