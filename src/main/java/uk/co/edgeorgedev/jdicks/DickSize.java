/**
 * DickSize.java
 * JDicks
 *
 * Created by Ed George on 7 Jan 2015
 *
 */
package uk.co.edgeorgedev.jdicks;

/**
 * @author edgeorge
 *
 */
public enum DickSize {
	BLACK, ASIAN, EUROPEAN;
	
	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
}
