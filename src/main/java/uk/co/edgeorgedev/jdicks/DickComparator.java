/**
 * DickComparitor.java
 * JDicks
 *
 * Created by Ed George on 7 Jan 2015
 *
 */
package uk.co.edgeorgedev.jdicks;

import java.util.Comparator;

/**
 * @author edgeorge
 *
 */
public class DickComparator implements Comparator<String> {

	public int compare(String dick1, String dick2) {
		return dick1.length() - dick2.length();
	}

}
