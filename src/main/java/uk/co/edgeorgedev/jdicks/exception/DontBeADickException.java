/**
 * DoneBeADickException.java
 * JDicks
 *
 * Created by Ed George on 7 Jan 2015
 *
 */
package uk.co.edgeorgedev.jdicks.exception;

/**
 * @author edgeorge
 *
 */
public class DontBeADickException extends Exception{

	private final static String prefix = "Don't be a dick: ";

	public DontBeADickException(){
		super();
	}
	
	public DontBeADickException(String message){
		super(prefix  + message);
	}	
	
	private static final long serialVersionUID = 1L;
		
}
