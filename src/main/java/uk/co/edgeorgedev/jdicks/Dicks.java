/**
 * Dicks.java
 * JDicks
 *
 * Created by Ed George on 7 Jan 2015
 *
 */
package uk.co.edgeorgedev.jdicks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

/**
 * @author edgeorge
 *
 */
public class Dicks {

	@Expose
	private List<String> dicks = new ArrayList<String>();

	private static Gson gson;

	static{
		gson = new GsonBuilder()
		.serializeNulls()
		.excludeFieldsWithoutExposeAnnotation()
		.disableHtmlEscaping()
		.create();
	}

	protected Dicks(List<String> dicks) {
		this.dicks = dicks;
	}

	public List<String> getAllDicks(){
		return dicks;
	}

	public String getRandomDick(){
		Random randomGenerator = new Random();
		int index = randomGenerator.nextInt(dicks.size());
		return dicks.get(index);
	}

	public String toJSON(){
		return gson.toJson(this);
	}

	@Override
	public String toString() {
		return dicks.toString();
	}

	public Dicks sort(){
		Collections.sort(dicks, new DickComparator());
		return this;
	}

}
