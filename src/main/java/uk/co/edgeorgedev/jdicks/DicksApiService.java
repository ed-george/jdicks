/**
 * DicksApiService.java
 * JDicks
 *
 * Created by Ed George on 7 Jan 2015
 *
 */
package uk.co.edgeorgedev.jdicks;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * @author edgeorge
 *
 */
public interface DicksApiService {
	
	@Headers("User-Agent: JDicks")
	@GET("/dicks/{quantity}")
	Dicks getBagOfDicks(@Path("quantity") int quantity);
	
	@Headers("User-Agent: JDicks")
	@GET("/dicks/{quantity}")
	Dicks getBagOfDicks(@Path("quantity") int quantity, @Query("size") String size);
	
	@Headers("User-Agent: JDicks")
	@GET("/dicks/{quantity}")
	void getBagOfDicks(@Path("quantity") int quantity, Callback<Dicks> callback);
	
	@Headers("User-Agent: JDicks")
	@GET("/dicks/{quantity}")
	void getBagOfDicks(@Path("quantity") int quantity, @Query("size") String size, Callback<Dicks> callback);
	
}
