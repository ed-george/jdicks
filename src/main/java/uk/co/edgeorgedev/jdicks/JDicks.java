/**
 * JDicks.java
 * JDicks
 *
 * Created by Ed George on 7 Jan 2015
 *
 */
package uk.co.edgeorgedev.jdicks;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import uk.co.edgeorgedev.jdicks.exception.DontBeADickException;

/**
 * @author edgeorge
 *
 */
public class JDicks {

	DicksApiService service;

	public JDicks() {
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://dicks-api.herokuapp.com/").build();
		service = restAdapter.create(DicksApiService.class);
	}

	public Dicks getDicks(int numberOfDicks) throws DontBeADickException{
		try{
			if(numberOfDicks < 1 || numberOfDicks > 9000){
				throw new DontBeADickException("You can't request " + Integer.toString(numberOfDicks) + " dicks");
			}
			return service.getBagOfDicks(numberOfDicks);
		}catch(RetrofitError e){
			e.printStackTrace();
			throw new DontBeADickException("Check your internet settings!");
		}
	}

	public void getDicks(int numberOfDicks, Callback<Dicks> callback) throws DontBeADickException{
		try{
			if(numberOfDicks < 1 || numberOfDicks > 9000){
				throw new DontBeADickException("You can't request " + Integer.toString(numberOfDicks) + " dicks");
			}
			service.getBagOfDicks(numberOfDicks, callback);
		}catch(RetrofitError e){
			throw new DontBeADickException("Check your internet settings!");
		}
	}

	public Dicks getSizedDicks(int numberOfDicks, DickSize size) throws DontBeADickException{
		try{
			if(numberOfDicks < 1 || numberOfDicks > 9000){
				throw new DontBeADickException("You can't request " + Integer.toString(numberOfDicks) + " dicks");
			}
			return service.getBagOfDicks(numberOfDicks, size.toString());
		}catch(RetrofitError e){
			throw new DontBeADickException("Check your internet settings!");
		}
	}

	public void getSizedDicks(int numberOfDicks, DickSize size, Callback<Dicks> callback) throws DontBeADickException{
		try{
			if(numberOfDicks < 1 || numberOfDicks > 9000){
				throw new DontBeADickException("You can't request " + Integer.toString(numberOfDicks) + " dicks");
			}
			service.getBagOfDicks(numberOfDicks, size.toString(), callback);
		}catch(RetrofitError e){
			throw new DontBeADickException("Check your internet settings!");
		}
	}
}
