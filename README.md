# README #

### What...what is this? ###

* A Java wrapper for the [Dicks API](https://dicks-api.herokuapp.com/)

### What/Who is this repository for? ###

* Dicks.

### How do I get set up? ###


```
#!java

JDicks jdicks = new JDicks();
Dicks dicks = jdicks.getDicks(10);
// Show all the dicks.
System.out.println(dicks);
```


